﻿using System;

namespace Roduso.Demo.Model
{
    public class Article : AggregateRoot
    {
        private Article() { }

        public Article(string name, double amount)
        {
            Id = Guid.NewGuid();

            Name = name;
            Amount = amount;
        }

        public string Name { get; private set; }
        public double Amount { get; private set; }
    }
}