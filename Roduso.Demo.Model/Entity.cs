﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Roduso.Demo.Model
{
    public abstract class Entity
    {
        [Key]
        public Guid Id { get; protected set; }

        protected Entity()
        {
            DomainEvents = new List<IDomainEvent>();
        }

        public ICollection<IDomainEvent> DomainEvents { get; private set; }

        public override bool Equals(object obj)
        {
            var entity = obj as Entity;
            return entity != null && Equals(entity);
        }

        protected bool Equals(Entity other)
        {
            return Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public List<IDomainEvent> FindAllDomainEvents()
        {
            var exploredObjects = new HashSet<object>();
            var found = new List<IDomainEvent>();

            FindAllDomainEvents(this, exploredObjects, found);

            return found;
        }

        private void FindAllDomainEvents<T>(object value, ISet<object> exploredObjects, ICollection<T> found) where T : class
        {
            if (value == null)
            {
                return;
            }

            if (exploredObjects.Contains(value))
            {
                return;
            }

            exploredObjects.Add(value);

            var enumerable = value as IEnumerable;

            if (enumerable != null)
            {
                foreach (var item in enumerable)
                {
                    FindAllDomainEvents(item, exploredObjects, found);
                }
            }
            else
            {
                var possibleMatch = value as T;

                if (possibleMatch != null)
                {
                    found.Add(possibleMatch);
                }

                Type type = value.GetType();

                PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty);

                foreach (PropertyInfo property in properties)
                {
                    var propertyValue = property.GetValue(value, null);

                    FindAllDomainEvents(propertyValue, exploredObjects, found);
                }
            }
        }
    }
}