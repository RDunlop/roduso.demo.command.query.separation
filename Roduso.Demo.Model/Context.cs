﻿using System.Data.Entity;
using System.Transactions;

namespace Roduso.Demo.Model
{
    [DbConfigurationType(typeof(DataConfiguration))]
    public class Context : DbContext
    {
        public Context() : base("RodusoDemo")
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                Database.CreateIfNotExists();
            }
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public void AddOrder(Order order)
        {
            Orders.Add(order);
        }

        public void AddArticle(Article article)
        {
            Articles.Add(article);
        }

        public void AddCustomer(Customer customer)
        {
            Customers.Add(customer);
        }
    }
}