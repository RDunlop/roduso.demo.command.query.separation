﻿using System;

namespace Roduso.Demo.Model
{
    public class OrderWasPlacedDomainEvent : IDomainEvent
    {
        public int OrderNumber { get; }
        public Guid OrderArticleId { get; }
        public int OrderNumberOfArticles { get; }
        public Guid OrderPlacedBy { get; }

        public OrderWasPlacedDomainEvent(Order order)
        {
            OrderNumber = order.Number;
            OrderArticleId = order.ArticleId;
            OrderNumberOfArticles = order.NumberOfArticles;
            OrderPlacedBy = order.PlacedBy;
        }
    }
}