﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Roduso.Demo.Model
{
    public class DataConfiguration : DbConfiguration
    {
        public DataConfiguration()
        {            
            SetExecutionStrategy("System.Data.SqlClient", () => new DefaultExecutionStrategy());
            SetDefaultConnectionFactory(new LocalDbConnectionFactory("MSSQLLocalDB"));
        }
    }
}