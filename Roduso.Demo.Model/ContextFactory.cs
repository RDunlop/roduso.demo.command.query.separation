﻿namespace Roduso.Demo.Model
{
    public class ContextFactory : IContextFactory
    {
        public Context CreateContext()
        {
            return new Context();
        }
    }
}