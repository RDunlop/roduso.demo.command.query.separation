﻿namespace Roduso.Demo.Model
{
    public interface IContextFactory
    {
        Context CreateContext();
    }
}