﻿namespace Roduso.Demo.Model
{
    public interface IOrderNumberSeries
    {
        int GetNext();
    }
}