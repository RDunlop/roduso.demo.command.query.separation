﻿using System;

namespace Roduso.Demo.Model
{
    public class Customer : AggregateRoot
    {
        private Customer() { }

        public Customer(string name, string email)
        {
            Id = Guid.NewGuid();

            Name = name;
            Email = email;
        }
        
        public string Name { get; set; }
        public string Email { get; set; }
    }
}