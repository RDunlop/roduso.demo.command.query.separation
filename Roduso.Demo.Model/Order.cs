﻿using System;

namespace Roduso.Demo.Model
{
    public class Order : AggregateRoot
    {
        public int Number { get; private set; }
        public Guid ArticleId { get; private set; }
        public int NumberOfArticles { get; private set; }
        public double TotalAmount { get; private set; }
        public Guid PlacedBy { get; private set; }
        public DateTime PlacedAt { get; private set; }
        public bool IsShipped { get; private set; }
        public bool IsSuspended { get; private set; }

        private Order() { }

        public Order(IOrderNumberSeries orderNumberSeries, Guid articleId, int numberOfArticles, double articleAmount, Guid placedBy)
        {
            ValidateArticle(articleId, numberOfArticles, articleAmount);

            Id = Guid.NewGuid();

            Number = orderNumberSeries.GetNext();
            ArticleId = articleId;
            TotalAmount = numberOfArticles * articleAmount;
            PlacedBy = placedBy;
            PlacedAt = DateTime.Now;

            DomainEvents.Add(new OrderWasPlacedDomainEvent(this));
        }

        public void ShipOrder()
        {
            if (IsSuspended)
            {
                throw new Exception("The order cannot be shipped since it is previously suspended");
            }

            IsShipped = true;
        }

        public void SuspendOrder()
        {
            if (IsShipped)
            {
                throw new Exception("The order cannot be suspended since it is previously shipped");
            }

            IsSuspended = true;
        }

        private void ValidateArticle(Guid articleId, int numberOfArticles, double articleAmount)
        {
            if (articleId == Guid.Empty)
            {
                throw new ArgumentException("An article must be given");
            }

            if (numberOfArticles <= 0)
            {
                throw new ArgumentException("Number of articles must be a positive value greater than 0");
            }

            if (articleAmount < 0)
            {
                throw new ArgumentException("The amount must be a positive value or equal to 0");
            }
        }
    }
}