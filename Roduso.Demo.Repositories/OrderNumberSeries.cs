﻿using System.Linq;
using Roduso.Demo.Model;

namespace Roduso.Demo.Repositories
{
    public class OrderNumberSeries : IOrderNumberSeries
    {
        public int GetNext()
        {
            using (var db = new Context())
            {
                var highestnumber = db.Orders.OrderByDescending(x => x.Number).SingleOrDefault()?.Number;
                return highestnumber ?? 1;
            }
        }
    }
}