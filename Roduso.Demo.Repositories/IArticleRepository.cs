﻿using System;
using Roduso.Demo.Model;

namespace Roduso.Demo.Repositories
{
    public interface IArticleRepository
    {
        Article LoadArticle(Guid articleId);
        void SaveArticle(Article article);
    }
}