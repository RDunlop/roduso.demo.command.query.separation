﻿using Roduso.Demo.Model;

namespace Roduso.Demo.Repositories
{
    public interface IOrderRepository
    {
        Order LoadOrder(int orderNumber);
        void SaveOrder(Order order);
    }
}