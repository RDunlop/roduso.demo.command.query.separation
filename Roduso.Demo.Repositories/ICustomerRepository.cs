﻿using System;
using Roduso.Demo.Model;

namespace Roduso.Demo.Repositories
{
    public interface ICustomerRepository
    {
        Customer LoadCustomer(Guid customerId);
        void SaveCustomer(Customer customer);
    }
}