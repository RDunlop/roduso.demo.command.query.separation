﻿using System.Collections.Generic;

namespace Roduso.Demo.Queries.OrderHandling.ShowPendingOrders
{
    public class ShowPendingOrdersViewData
    {
        public List<PendingOrderViewData> PendingOrders { get; set; }
    }
}