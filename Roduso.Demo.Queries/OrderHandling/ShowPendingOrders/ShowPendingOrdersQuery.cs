﻿
using System.Linq;
using Roduso.Demo.Model;

namespace Roduso.Demo.Queries.OrderHandling.ShowPendingOrders
{
    public class ShowPendingOrdersQuery
    {
        private readonly IContextFactory _contextFactory;

        public ShowPendingOrdersQuery(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public ShowPendingOrdersViewData Execute()
        {
            using (var db = _contextFactory.CreateContext())
            {
                var pendingOrders =
                    db
                    .Orders
                    .Where(o => o.IsShipped == false)
                    .Select(pendingOrder => new PendingOrderViewData
                    {
                        OrderNumber = pendingOrder.Number,
                        ArticleName = db.Articles.Single(a => a.Id == pendingOrder.ArticleId).Name,
                        NumberOfArticles = pendingOrder.NumberOfArticles,
                        TotalAmount = pendingOrder.TotalAmount
                    })
                    .ToList();

                return new ShowPendingOrdersViewData
                {
                    PendingOrders = pendingOrders
                };
            }
        }
    }
}