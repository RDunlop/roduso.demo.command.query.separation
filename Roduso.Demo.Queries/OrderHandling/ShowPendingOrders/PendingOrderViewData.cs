﻿namespace Roduso.Demo.Queries.OrderHandling.ShowPendingOrders
{
    public class PendingOrderViewData
    {
        public int OrderNumber { get; set; }
        public string ArticleName { get; set; }
        public int NumberOfArticles { get; set; }
        public double TotalAmount { get; set; }
    }
}