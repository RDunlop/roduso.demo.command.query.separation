﻿using System.Collections.Generic;

namespace Roduso.Demo.Specifications
{
    public interface IInstanceCollector
    {
        List<T> FindAllInstances<T>(object value) where T : class;
    }
}