﻿using Roduso.Demo.Specifications.DomainEvents.PlainEntity;

namespace Roduso.Demo.Specifications.DomainEvents.EntityWithInnerEntity
{
    public class EntityWithInnerEntity : FakeEntity
    {
        public EntityWithInnerEntity()
        {
            InnerEntity = new FakeEntity();
        }

        public FakeEntity InnerEntity { get; set; }
    }
}