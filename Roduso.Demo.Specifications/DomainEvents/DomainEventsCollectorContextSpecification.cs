﻿using System.Collections.Generic;
using Roduso.Demo.Model;
using Roduso.Demo.Specifications.DomainEvents.PlainEntity;

namespace Roduso.Demo.Specifications.DomainEvents
{
    public abstract class DomainEventsCollectorContextSpecification : ContextSpecification
    {
        protected FakeEntity Entity { get; set; }
        protected ICollection<IDomainEvent> FoundDomainEnvents { get; set; }

        protected override void CreateMocks()
        {
        }

        protected override void Givens()
        {
        }

        protected override void SetupMockBehaviour()
        {
        }

        protected void CreateAnEntity()
        {
            Entity = new FakeEntity();
        }

        protected void CreateAnEntityWithAnInnerEntity()
        {
            Entity = new EntityWithInnerEntity.EntityWithInnerEntity();
        }

        protected void CreateAnEntityWithAListOfEntities()
        {
            Entity = new EntityWithListOfInnerEntities.EntityWithListOfInnerEntities();
        }

        protected void AlterAllEntitiesInGraphContainADomainEvent()
        {
            var entities = new InstanceCollector().FindAllInstances<FakeEntity>(Entity);
            entities.ForEach(x=>x.AlterTheEntitySoThatADomainEventIsCreated());
        }
    }
}