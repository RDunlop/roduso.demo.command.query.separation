﻿using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;
using Roduso.Demo.Specifications.DomainEvents.PlainEntity;

namespace Roduso.Demo.Specifications.DomainEvents.EntityWithListOfInnerEntities
{
    public class EntityWithListOfInnerEntities : FakeEntity
    {
        public EntityWithListOfInnerEntities()
        {
            InnerEntities = new EditableList<FakeEntity>();

            InnerEntities.Add(new FakeEntity());
            InnerEntities.Add(new FakeEntity());
        }

        public List<FakeEntity> InnerEntities { get; set; }
    }
}