﻿using NUnit.Framework;
using Roduso.Demo.Model;
using Roduso.Demo.Specifications.DomainEvents.PlainEntity;

namespace Roduso.Demo.Specifications.DomainEvents.EntityWithListOfInnerEntities
{
    [TestFixture]
    public class WhenDomainEventsAreCollected : DomainEventsCollectorContextSpecification
    {
        [Test]
        public void ADomainEventPerEntityInTheGraphShouldBeFound()
        {
            var entities = new InstanceCollector().FindAllInstances<FakeEntity>(Entity);
            Assert.That(FoundDomainEnvents.Count, Is.GreaterThan(1).And.EqualTo(entities.Count));
        }

        protected override void Givens()
        {
        }

        protected override void InitializeStateOnTestClass()
        {
            CreateAnEntityWithAListOfEntities();
            AlterAllEntitiesInGraphContainADomainEvent();
        }

        protected override void When()
        {
            FoundDomainEnvents = Entity.FindAllDomainEvents();
        }
    }
}