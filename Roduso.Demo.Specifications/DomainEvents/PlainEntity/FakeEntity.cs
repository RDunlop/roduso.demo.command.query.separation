﻿using System;
using Roduso.Demo.Model;
using Roduso.Demo.Specifications.DomainEvents.FakeEntities;

namespace Roduso.Demo.Specifications.DomainEvents.PlainEntity
{
    public class FakeEntity : Entity
    {
        public FakeEntity()
        {
            Id = Guid.NewGuid();
        }

        public void AlterTheEntitySoThatADomainEventIsCreated()
        {
            DomainEvents.Add(new FakeEntityWasAltered());
        }
    }
}