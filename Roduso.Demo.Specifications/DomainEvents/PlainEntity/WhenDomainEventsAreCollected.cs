﻿using NUnit.Framework;
using Roduso.Demo.Model;

namespace Roduso.Demo.Specifications.DomainEvents.PlainEntity
{
    [TestFixture]
    public class WhenDomainEventsAreCollected : DomainEventsCollectorContextSpecification
    {
        [Test]
        public void ADomainEventPerEntityInTheGraphShouldBeFound()
        {
            var entities = new InstanceCollector().FindAllInstances<FakeEntity>(Entity);
            Assert.That(FoundDomainEnvents.Count, Is.GreaterThan(0).And.EqualTo(entities.Count));
        }

        protected override void InitializeStateOnTestClass()
        {
            CreateAnEntity();
            AlterAllEntitiesInGraphContainADomainEvent();
        }

        protected override void When()
        {
            FoundDomainEnvents = Entity.FindAllDomainEvents();
        }
    }
}