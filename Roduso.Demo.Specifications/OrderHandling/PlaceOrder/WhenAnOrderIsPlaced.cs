﻿using System.Linq;
using Moq;
using NUnit.Framework;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using Roduso.Demo.Model;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder
{
    public class WhenAnOrderIsPlaced : PlaceOrderCommandHandlerContextSpecification
    {
        [Test]
        public void AnOrderShouldBeCreated()
        {
            OrderFactoryMock.Verify(x => x.CreateOrder(It.IsAny<PlaceOrderCommand>()), Times.Once);
        }

        [Test]
        public void TheOrderShouldBePersisted()
        {
            OrderRepositoryMock.Verify(x => x.SaveOrder(It.IsAny<Order>()), Times.Once);
        }

        [Test]
        public void AnOrderWasPlacedDomainEventShouldBePublished()
        {
            Assert.That(TestDomainEventsSubscriber.DomainEvents.Any(x => x is OrderWasPlacedDomainEvent));
        }

        protected override void Givens()
        {
            GivenAnOrderIsPlaced();
            GivenADomainEventSubscriber();
        }

        protected override void SetupMockBehaviour()
        {
            SimulateOrderCreation();
            SimulateSavingAnOrder();
        }

        protected override void InitializeStateOnTestClass()
        {
            PlaceOrderCommandHandler = CreateCommandHandler();
        }

        protected override void When()
        {
            PlaceOrderCommandHandler.PlaceOrder(PlaceOrderCommand);
        }
    }
}