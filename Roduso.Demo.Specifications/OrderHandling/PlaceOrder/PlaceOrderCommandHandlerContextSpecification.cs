﻿using System;
using Moq;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using Roduso.Demo.Model;
using Roduso.Demo.Repositories;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder
{
    public abstract class PlaceOrderCommandHandlerContextSpecification : ContextSpecification
    {
        protected IPlaceOrderCommandHandler PlaceOrderCommandHandler { get; set; }

        protected Mock<IOrderFactory> OrderFactoryMock { get; set; }
        protected Mock<IOrderRepository> OrderRepositoryMock { get; set; }
        protected Mock<IOrderNumberSeries> OrderNumberSeriesMock { get; set; }

        protected TestDomainEventsSubscriber TestDomainEventsSubscriber { get; set; }

        protected PlaceOrderCommand PlaceOrderCommand { get; set; }

        protected override void CreateMocks()
        {
            OrderFactoryMock = new Mock<IOrderFactory>(MockBehavior.Strict);
            OrderRepositoryMock = new Mock<IOrderRepository>(MockBehavior.Strict);
            OrderNumberSeriesMock = new Mock<IOrderNumberSeries>(MockBehavior.Strict);            
        }

        protected PlaceOrderCommandHandler CreateCommandHandler()
        {
            return new PlaceOrderCommandHandler(
                OrderFactoryMock.Object,
                OrderRepositoryMock.Object,
                TestDomainEventsSubscriber);
        }

        protected void GivenADomainEventSubscriber()
        {
            TestDomainEventsSubscriber = new TestDomainEventsSubscriber();
        }

        protected void GivenAnOrderIsPlaced()
        {
            PlaceOrderCommand = new PlaceOrderCommand();
        }

        protected void SimulateOrderCreation()
        {
            OrderNumberSeriesMock.Setup(x => x.GetNext()).Returns(3);
            var newOrder = new Order(OrderNumberSeriesMock.Object, articleId: Guid.NewGuid(), numberOfArticles: 2, articleAmount: 2.3, placedBy: Guid.NewGuid());
            OrderFactoryMock.Setup(x => x.CreateOrder(It.IsAny<PlaceOrderCommand>())).Returns(newOrder);
        }

        protected void SimulateSavingAnOrder()
        {
            OrderRepositoryMock.Setup(x => x.SaveOrder(It.IsAny<Order>())).Verifiable();
        }
    }
}