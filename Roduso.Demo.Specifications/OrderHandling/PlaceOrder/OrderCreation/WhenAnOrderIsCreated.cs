﻿using NUnit.Framework;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using Roduso.Demo.Model;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder.OrderCreation
{
    public class WhenAnOrderIsCreated : OrderFactoryContextSpecification
    {
        private Order _newOrder;

        [Test]
        public void ANewOrderShouldBeCreatedAndAvailable()
        {
            Assert.That(_newOrder, Is.Not.Null);
        }

        protected override void Givens()
        {
            Article = GivenAnArticle(amount: 1.2);
            GivenAUserPlacesAnOrder(article: Article, numberOfArticles: 1);
        }

        protected override void SetupMockBehaviour()
        {
            SimulateGenerationOfANewOrderNumber();
            SimulateLoadingAnArticle();
            SimulateRetreivingTheCallingUser();
        }

        protected override void InitializeStateOnTestClass()
        {
            OrderFactory = new OrderFactory(OrderNumberSeriesMock.Object, ArticleRepositoryMock.Object, PrincipalMock.Object);
        }

        protected override void When()
        {
            _newOrder = OrderFactory.CreateOrder(PlaceOrderCommand);
        }
    }
}