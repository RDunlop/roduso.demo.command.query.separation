﻿using System;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using Roduso.Demo.Model;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder.OrderCreation
{
    public class WhenTheUserTriesToCreateAnOrderWithoutAnArticle : OrderFactoryContextSpecification
    {
        private Order _newOrder;

        [Test]
        public void AnExceptionShouldBeThrown()
        {
            Assert.That(ActualException, Is.TypeOf(ExpectedExceptionType));
        }

        [Test]
        public void NoNewOrderShouldBeCreated()
        {
            Assert.That(_newOrder, Is.Null);
        }

        [Test]
        public void TheActualExceptionShouldStateThatAnArticleIsMissing()
        {
            Assert.That(ActualException.Message, Does.StartWith("There is no article corresponding to "));
        }

        protected override void Givens()
        {
            GivenAUserPlacesAnOrder(article: null, numberOfArticles: 1);
        }

        public override Type ExpectedExceptionType => typeof(ArgumentException);

        protected override void SetupMockBehaviour()
        {
            SimulateGenerationOfANewOrderNumber();
            SimulateLoadingAnArticle();
            SimulateRetreivingTheCallingUser();
        }

        protected override void InitializeStateOnTestClass()
        {
            OrderFactory = new OrderFactory(OrderNumberSeriesMock.Object, ArticleRepositoryMock.Object, PrincipalMock.Object);
        }

        protected override void When()
        {
            _newOrder = OrderFactory.CreateOrder(PlaceOrderCommand);
        }
    }
}