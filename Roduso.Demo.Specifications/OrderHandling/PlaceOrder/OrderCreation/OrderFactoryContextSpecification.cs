﻿using System;
using Moq;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using Roduso.Demo.Model;
using Roduso.Demo.Repositories;
using Roduso.Demo.Security;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder.OrderCreation
{
    public abstract class OrderFactoryContextSpecification : ContextSpecification
    {
        protected IOrderFactory OrderFactory { get; set; }

        protected Mock<IOrderNumberSeries> OrderNumberSeriesMock { get; set; }
        protected Mock<IArticleRepository> ArticleRepositoryMock { get; set; }
        protected Mock<IPrincipal> PrincipalMock { get; set; }
        protected PlaceOrderCommand PlaceOrderCommand { get; set; }
        protected Article Article { get; set; }

        protected override void CreateMocks()
        {
            OrderNumberSeriesMock = new Mock<IOrderNumberSeries>(MockBehavior.Strict);
            ArticleRepositoryMock = new Mock<IArticleRepository>(MockBehavior.Strict);
            PrincipalMock = new Mock<IPrincipal>(MockBehavior.Strict);
        }

        protected void SimulateGenerationOfANewOrderNumber()
        {
            OrderNumberSeriesMock.Setup(x => x.GetNext()).Returns(4);
        }

        protected void SimulateLoadingAnArticle()
        {
            ArticleRepositoryMock.Setup(x => x.LoadArticle(It.IsAny<Guid>())).Returns(Article);
        }

        protected void SimulateRetreivingTheCallingUser()
        {
            PrincipalMock.Setup(x => x.GetUserId()).Returns(Guid.NewGuid());
        }

        protected void GivenAUserPlacesAnOrder(Article article, int numberOfArticles = 0)
        {
            PlaceOrderCommand = new PlaceOrderCommand { ArticleId = article?.Id ?? Guid.Empty, NumberOfArticles = numberOfArticles };
        }

        protected Article GivenAnArticle(double amount = 0, string name = "Any old name")
        {
            Article = new Article(name, amount);
            return Article;
        }
    }
}