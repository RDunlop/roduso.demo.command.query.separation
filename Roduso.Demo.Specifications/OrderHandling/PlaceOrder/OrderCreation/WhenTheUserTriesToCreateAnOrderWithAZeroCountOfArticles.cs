﻿using System;
using NUnit.Framework;
using Roduso.Demo.Commands.Orderhandling.PlaceOrder;
using Roduso.Demo.Model;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder.OrderCreation
{
    public class WhenTheUserTriesToCreateAnOrderWithAZeroCountOfArticles : OrderFactoryContextSpecification
    {
        private Order _newOrder;

        [Test]
        public void AnExceptionShouldBeThrown()
        {
            Assert.That(ActualException, Is.TypeOf(ExpectedExceptionType));
        }

        [Test]
        public void NoNewOrderShouldBeCreated()
        {
            Assert.That(_newOrder, Is.Null);
        }

        [Test]
        public void TheActualExceptionShouldStateThatTheNumberOfArticlesMostBeAPositiveNumber()
        {
            Assert.That(ActualException.Message, Is.EqualTo("Number of articles must be a positive value greater than 0"));
        }

        protected override void Givens()
        {
            Article = GivenAnArticle(amount: 1.2);
            GivenAUserPlacesAnOrder(article: Article, numberOfArticles: 0);
        }

        public override Type ExpectedExceptionType => typeof(ArgumentException);

        protected override void SetupMockBehaviour()
        {
            SimulateGenerationOfANewOrderNumber();
            SimulateLoadingAnArticle();
            SimulateRetreivingTheCallingUser();
        }

        protected override void InitializeStateOnTestClass()
        {
            OrderFactory = new OrderFactory(OrderNumberSeriesMock.Object, ArticleRepositoryMock.Object, PrincipalMock.Object);
        }

        protected override void When()
        {
            _newOrder = OrderFactory.CreateOrder(PlaceOrderCommand);
        }
    }
}