﻿using System;
using System.Transactions;
using Moq;
using Roduso.Demo.Model;
using Roduso.Demo.Repositories;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder.OrderPersistence
{
    public abstract class OrderRepositoryContextSpecification : ContextSpecification
    {
        protected IOrderRepository OrderRepository { get; set; }

        protected Mock<IContextFactory> ContextFactoryMock { get; set; }
        protected Mock<IOrderNumberSeries> OrderNumberSeriesMock { get; set; }

        protected Order Order { get; set; }
        protected int OrderNumber { get; set; }

        protected override void CreateMocks()
        {
            ContextFactoryMock = new Mock<IContextFactory>(MockBehavior.Strict);
            OrderNumberSeriesMock = new Mock<IOrderNumberSeries>(MockBehavior.Strict);
        }

        protected void GivenAnOrder()
        {
            SimulateTheNumberSeries();
            Order = new Order(OrderNumberSeriesMock.Object, Guid.NewGuid(), 1, 1, Guid.NewGuid());
        }

        protected void SimulateTheNumberSeries()
        {
            OrderNumber = 234432;
            OrderNumberSeriesMock.Setup(x => x.GetNext()).Returns(OrderNumber);
        }

        protected void SimulateTheContextFactoryToUseAProperDatabase()
        {
            ContextFactoryMock.Setup(x => x.CreateContext()).Returns(() => new Context());
        }
    }
}