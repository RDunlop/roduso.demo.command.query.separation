﻿using NUnit.Framework;
using Roduso.Demo.Model;
using Roduso.Demo.Repositories;

namespace Roduso.Demo.Specifications.OrderHandling.PlaceOrder.OrderPersistence
{
    [Category("Integration")]
    public class WhenAnOrderIsSaved : OrderRepositoryContextSpecification
    {
        private Order _loadedOrder;

        [Test]
        public void ItShouldBePossibleToLoadTheSavedOrder()
        {            
            Assert.That(_loadedOrder, Is.EqualTo(Order));
        }

        protected override void GetSavedTestData()
        {
            _loadedOrder = OrderRepository.LoadOrder(OrderNumber);
        }

        protected override void Givens()
        {
            GivenAnOrder();
        }

        protected override void SetupMockBehaviour()
        {
            SimulateTheContextFactoryToUseAProperDatabase();
        }

        protected override void InitializeStateOnTestClass()
        {
            OrderRepository = new OrderRepository(ContextFactoryMock.Object);
        }

        protected override void When()
        {
            OrderRepository.SaveOrder(Order);
        }
    }
}