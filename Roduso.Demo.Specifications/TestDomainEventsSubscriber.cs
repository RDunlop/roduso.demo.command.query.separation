﻿using System.Collections.Generic;
using Roduso.Demo.Commands;
using Roduso.Demo.Model;

namespace Roduso.Demo.Specifications
{
    public class TestDomainEventsSubscriber : IDomainEventsSubscriber
    {
        public List<IDomainEvent> DomainEvents { get; private set; }

        public void Receive(List<IDomainEvent> domainEvents)
        {
            DomainEvents = domainEvents;
        }
    }
}