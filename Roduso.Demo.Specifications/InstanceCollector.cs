﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Roduso.Demo.Specifications
{
    public class InstanceCollector : IInstanceCollector
    {
        public List<T> FindAllInstances<T>(object value) where T : class
        {
            var exploredObjects = new HashSet<object>();
            var found = new List<T>();

            FindAllInstances(value, exploredObjects, found);

            return found;
        }

        private void FindAllInstances<T>(object value, ISet<object> exploredObjects, ICollection<T> found) where T : class
        {
            if (value == null)
            {
                return;
            }

            if (exploredObjects.Contains(value))
            {
                return;
            }

            exploredObjects.Add(value);

            var enumerable = value as IEnumerable;

            if (enumerable != null)
            {
                foreach (object item in enumerable)
                {
                    FindAllInstances<T>(item, exploredObjects, found);
                }
            }
            else
            {
                var possibleMatch = value as T;

                if (possibleMatch != null)
                {
                    found.Add(possibleMatch);
                }

                Type type = value.GetType();

                PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty);

                foreach (PropertyInfo property in properties)
                {
                    object propertyValue = property.GetValue(value, null);

                    FindAllInstances<T>(propertyValue, exploredObjects, found);
                }
            }
        }
    }
}