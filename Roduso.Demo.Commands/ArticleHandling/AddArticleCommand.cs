namespace Roduso.Demo.Commands.ArticleHandling
{
    public class AddArticleCommand
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }
}