using System;

namespace Roduso.Demo.Commands.ArticleHandling
{
    public class AddArticleResponse
    {
        public Guid NewArticleId { get; set; }
    }
}