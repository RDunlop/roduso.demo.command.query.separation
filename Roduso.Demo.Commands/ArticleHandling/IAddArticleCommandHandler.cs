namespace Roduso.Demo.Commands.ArticleHandling
{
    public interface IAddArticleCommandHandler
    {
        AddArticleResponse AddArticle(AddArticleCommand addArticleCommand);
    }
}