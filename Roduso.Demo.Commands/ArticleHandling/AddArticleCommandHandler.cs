﻿using Roduso.Demo.Model;
using Roduso.Demo.Repositories;

namespace Roduso.Demo.Commands.ArticleHandling
{
    public class AddArticleCommandHandler : IAddArticleCommandHandler
    {
        private readonly IArticleRepository _articleRepository;

        public AddArticleCommandHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public AddArticleResponse AddArticle(AddArticleCommand addArticleCommand)
        {
            var newArticle = new Article(addArticleCommand.Name, addArticleCommand.Amount);

            _articleRepository.SaveArticle(newArticle);

            return new AddArticleResponse {NewArticleId = newArticle.Id};
        }
    }
}