using System.Collections.Generic;
using Roduso.Demo.Model;

namespace Roduso.Demo.Commands
{
    public interface IDomainEventsSubscriber
    {
        void Receive(List<IDomainEvent> domainEvents);
    }
}