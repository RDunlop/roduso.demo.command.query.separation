﻿using Roduso.Demo.Repositories;
using Roduso.Demo.Security;

namespace Roduso.Demo.Commands.Communication
{
      public class Communicator : ICommunicator
    {
        private readonly IPrincipal _principal;
        private readonly ICustomerRepository _customerRepository;

        public Communicator(IPrincipal principal, ICustomerRepository customerRepository)
        {
            _principal = principal;
            _customerRepository = customerRepository;
        }

        public void NotifyCustomer(string message)
        {
            var customerId = _principal.GetUserId().Value;
            var customer = _customerRepository.LoadCustomer(customerId);
            string email = customer.Email;
        }
    }
}