﻿namespace Roduso.Demo.Commands.Communication
{
    public interface ICommunicator
    {
        void NotifyCustomer(string message);
    }
}