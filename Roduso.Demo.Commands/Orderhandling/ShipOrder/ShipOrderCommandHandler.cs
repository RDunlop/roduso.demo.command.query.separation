﻿using Roduso.Demo.Repositories;

namespace Roduso.Demo.Commands.Orderhandling.ShipOrder
{
    public class ShipOrderCommandHandler : IShipOrderCommandHandler
    {
        private readonly IOrderRepository _orderRepository;

        public ShipOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void ShipOrder(ShipOrderCommand shipOrderCommand)
        {
            var order = _orderRepository.LoadOrder(shipOrderCommand.OrderNumber);

            order.ShipOrder();

            _orderRepository.SaveOrder(order);
        }
    }
}