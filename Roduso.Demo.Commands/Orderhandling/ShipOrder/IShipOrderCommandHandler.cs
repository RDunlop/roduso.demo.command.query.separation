﻿namespace Roduso.Demo.Commands.Orderhandling.ShipOrder
{
    public interface IShipOrderCommandHandler
    {
        void ShipOrder(ShipOrderCommand shipOrderCommand);
    }
}