using System;
using Roduso.Demo.Model;
using Roduso.Demo.Repositories;
using Roduso.Demo.Security;

namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public class OrderFactory : IOrderFactory
    {
        private readonly IOrderNumberSeries _orderNumberSeries;
        private readonly IArticleRepository _articleRepository;
        private readonly IPrincipal _principal;

        public OrderFactory(
            IOrderNumberSeries orderNumberSeries,
            IArticleRepository articleRepository,
            IPrincipal principal)
        {
            _orderNumberSeries = orderNumberSeries;
            _articleRepository = articleRepository;
            _principal = principal;
        }

        public Order CreateOrder(PlaceOrderCommand placeOrderCommand)
        {
            var userId = _principal.GetUserId();
            var article = _articleRepository.LoadArticle(placeOrderCommand.ArticleId);

            if (article == null)
            {
                throw new ArgumentException("There is no article corresponding to " + placeOrderCommand.ArticleId);
            }

            Order newOrder =
                new Order(
                    orderNumberSeries: _orderNumberSeries,
                    articleId: placeOrderCommand.ArticleId,
                    numberOfArticles: placeOrderCommand.NumberOfArticles,
                    articleAmount: article?.Amount ?? -1,
                    placedBy: userId.Value);
            
            return newOrder;
        }
    }
}