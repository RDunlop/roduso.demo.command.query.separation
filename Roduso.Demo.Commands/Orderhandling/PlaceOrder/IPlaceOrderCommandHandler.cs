namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public interface IPlaceOrderCommandHandler
    {
        void PlaceOrder(PlaceOrderCommand placeOrderCommand);
    }
}