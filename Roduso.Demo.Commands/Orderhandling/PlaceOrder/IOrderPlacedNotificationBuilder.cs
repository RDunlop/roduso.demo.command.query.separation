using Roduso.Demo.Model;

namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public interface IOrderPlacedNotificationBuilder
    {
        string ConstructOrderPlacedNotificationMessage(Order order);
    }
}