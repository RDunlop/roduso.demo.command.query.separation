using Roduso.Demo.Repositories;

namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public class PlaceOrderCommandHandler : IPlaceOrderCommandHandler
    {
        private readonly IOrderFactory _orderFactory;
        private readonly IOrderRepository _orderRepository;
        private readonly IDomainEventsSubscriber _domainEventsSubscriber;

        public PlaceOrderCommandHandler(
            IOrderFactory orderFactory,
            IOrderRepository orderRepository,
            IDomainEventsSubscriber domainEventsSubscriber)
        {
            _orderFactory = orderFactory;
            _orderRepository = orderRepository;
            _domainEventsSubscriber = domainEventsSubscriber;
        }

        public void PlaceOrder(PlaceOrderCommand placeOrderCommand)
        {
            var newOrder = _orderFactory.CreateOrder(placeOrderCommand);
            _orderRepository.SaveOrder(newOrder);
            _domainEventsSubscriber?.Receive(newOrder.FindAllDomainEvents());
        }
    }
}