﻿using System;

namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public class PlaceOrderCommand
    {
        public Guid ArticleId { get; set; }
        public int NumberOfArticles { get; set; }
    }
}