using Roduso.Demo.Model;

namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public interface IOrderFactory
    {
        Order CreateOrder(PlaceOrderCommand placeOrderCommand);
    }
}