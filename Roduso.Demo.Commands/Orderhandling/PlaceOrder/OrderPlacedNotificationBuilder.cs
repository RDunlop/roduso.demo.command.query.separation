using Roduso.Demo.Model;
using Roduso.Demo.Repositories;
using Roduso.Demo.Security;

namespace Roduso.Demo.Commands.Orderhandling.PlaceOrder
{
    public class OrderPlacedNotificationBuilder : IOrderPlacedNotificationBuilder
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IArticleRepository _articleRepository;
        private readonly IPrincipal _principal;

        public OrderPlacedNotificationBuilder(
            ICustomerRepository customerRepository,
            IArticleRepository articleRepository,
            IPrincipal principal)
        {
            _customerRepository = customerRepository;
            _articleRepository = articleRepository;
            _principal = principal;
        }

        public string ConstructOrderPlacedNotificationMessage(Order order)
        {
            var userId = _principal.GetUserId();

            var customer = _customerRepository.LoadCustomer(userId.Value);
            return "An order was placed by " + (customer?.Name ?? "(customer missing)") + " containing " + order.NumberOfArticles + " items of the article " + _articleRepository.LoadArticle(order.ArticleId).Name;
        }
    }
}