﻿using Roduso.Demo.Repositories;

namespace Roduso.Demo.Commands.Orderhandling.SuspendOrder
{
    public class SuspendOrderCommandHandler : ISuspendOrderCommandHandler
    {
        private readonly IOrderRepository _orderRepository;

        public SuspendOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void SuspendOrder(SuspendOrderCommand suspendOrderCommand)
        {
            var order = _orderRepository.LoadOrder(suspendOrderCommand.OrderNumber);

            order.SuspendOrder();

            _orderRepository.SaveOrder(order);
        }
    }
}