﻿namespace Roduso.Demo.Commands.Orderhandling.SuspendOrder
{
    public class SuspendOrderCommand
    {
        public int OrderNumber { get; set; }
    }
}