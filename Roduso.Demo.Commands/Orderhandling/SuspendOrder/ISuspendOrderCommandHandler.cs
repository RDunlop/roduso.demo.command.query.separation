﻿namespace Roduso.Demo.Commands.Orderhandling.SuspendOrder
{
    public interface ISuspendOrderCommandHandler
    {
        void SuspendOrder(SuspendOrderCommand suspendOrderCommand);
    }
}