﻿using System;

namespace Roduso.Demo.Security
{
    public class Principal : IPrincipal
    {
        public Guid? GetUserId()
        {
            return Guid.Empty;
        }
    }
}