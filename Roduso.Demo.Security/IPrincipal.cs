﻿using System;

namespace Roduso.Demo.Security
{
    public interface IPrincipal
    {
        Guid? GetUserId();
    }
}